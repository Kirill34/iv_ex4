#include <iostream>
#define ARRAY_SIZE 100

int pairCount(int arr[ARRAY_SIZE])
{
	int res = 0;
	for (int i = 1; i < ARRAY_SIZE; i++)
	{
		res += (arr[i] == arr[i - 1]) ? 1 : 0;
	}
	return res;
}

int main()
{
	setlocale(0, ".1251");
	std::cout << "������� 100 ��������� �������";
	int arr[ARRAY_SIZE];
	for (int i = 0; i < ARRAY_SIZE; i++)
	{
		std::cin >> arr[i];
	}
	std::cout << "���������� ���������������� ��� ���������� ���������: " << pairCount(arr);

	return 0;
}

